// Simple minifier web application for javascript, html and css
// Copyright (c) 2015 Guillaume Adam  http://www.galmiza.net/

/*
 * Simple minifier web service for javascript, html and css
 * Parameter: port to listen to
 * Request: HTTP POST in JSON { type: 'css|js|html', code: 'code to minify' }
 * Response: 'minified code'
 *
 * Usage: node simple-minifier-server.js <port>
 * Test:  curl -X POST http://localhost:<port> -d '{"var x=4+4;", "type":"js"}'
 */

// Import modules
var http = require('http');
var uglify = require('uglify-js');
var minify = require('html-minifier').minify;

// Get port from parameters
var port = parseInt(process.argv[2]);

// Create a server to receive and process minification requests
var server = http.createServer(function (request, response) {

  // Manage POST requests
  if (request.method == 'POST') {

    var body = '';
    request.on('data', function (data) {  body += data;   });
    request.on('end', function () {

      // Surround by try catch to prevent crashing the process (only 1 in nodejs!)
      try {

        // Process parameters
        var input = JSON.parse(body);
        var ext = '.'+input.type; // to keep the exact same code than simple-minifier-cli.js
        var code = input.code;

        // Tweak css content to make it a valid html tag with <style>
        // So that it can be fully minified by html-minifier
        if (ext=='.css')
          code = '<style>'+code+'</style>';

        // Minify
        var minified;
        if (ext=='.js') 
          minified = uglify.minify(code, {fromString: true}).code;
        if (ext=='.html' || ext=='.htm' || ext=='.css')
          minified = minify(code, {  
            removeComments: true,
            collapseWhitespace: true,
            minifyCSS: true,
            minifyJS: true });

        // Remove css <style> tag that we previously added
        if (ext=='.css')
          minified = minified.match(/<style>(.*?)<\/style>/)[1];

        // On success
        response.writeHead(200, {
          'Content-Type': 'text/plain',
          'Access-Control-Allow-Origin': '*' // required by web browsers to process the request (see CORS)
        });
        response.end(minified);

      // On error, just reply error code 500 (could be improved of course)
      } catch (e) {
        console.error(e);
        response.writeHead(500, {"Content-Type": "text/plain"});
        response.end("500 Internal Server Error\n");
      }
    });

  // If other method is received (like GET, PUT, DELETE) return method error
  } else {
    response.writeHead(405, {"Content-Type": "text/plain"});
    response.end("405 Method Not Allowed\n");
  }
  
});

// Listen on requested port and intercept error
server.on('error', function (err) {
  console.error(err);
  console.log("You may consider using another port");
});
server.listen(port);
console.log("Listening on port "+port);