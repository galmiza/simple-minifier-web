# Introduction

Simple-minifier-web is a web application that lets you minify your web resources.  
It supports **javascript**, **html** and **css**.  

Try it here: http://www.galmiza.net/git/simple-minifier-web/client

# Requirements

Simple-minifier-web comes with two components:  

  * a server as a [nodejs](https://nodejs.org/) application and using the modules
    * [uglify-js](https://github.com/mishoo/UglifyJS)
    * [html-minifier](https://github.com/kangax/html-minifier)  

  * a web client using [jQuery](https://jquery.com/)
  
# Installation

## Server

1/ Copy simple-minifier-server.js in your server  
2/ Install the required modules using the following commands  

```
$ cd /path/to/the/file
$ npm install uglify-js
$ npm install html-minifier
```

3/ Run the server and put an available port as parameter

```
$ cd /path/to/the/file
$ node simple-minifier-server.js 8080
```

**Note:** You can (and should) run the application with [forever](https://www.npmjs.com/package/forever) if you need the application to rerun automatically if it eventually crashes.

## Client

1/ Copy index.html in your local drive  
2/ Open it and change the hostname and port number (line 25) so that it matches your server configuration  

# Usage

Open index.html in a web browser.  
